.. git-compose documentation master file, created by
   sphinx-quickstart on Mon Mar 30 06:17:18 2020.
Compose for Git Documentation
=======================================

`Compose for Git <https://git-compose.gitlab.io/>`_ is a tool to 
manage several local `Git <https://git-scm.com/>`_ repositories. 
It should come handy in multirepository
projects, where one team works on a several services
and/or libraries often making changes in several of them.

.. role:: bash(code)
   :language: bash


Compose for Git provides you with CLI command :bash:`gico`,
and supports a specific **yaml** format describing
the structure of your project.

Imagine you have **git-compose.yaml** file like this one:

.. code-block:: yaml

   version: "0.1"
   repos:
     git-compose: 
       remote: https://gitlab.com/git-compose/git-compose
     git-compose-debian: 
       remote: https://gitlab.com/git-compose/git-compose-debian.git
     git-compose-vscode: 
       remote: https://gitlab.com/git-compose/git-compose-vscode
     git-compose-docs: 
       remote: https://gitlab.com/git-compose/git-compose-docs

Now all you need is to run :bash:`gico up` and you'll have all
of those repositories cloned in subfolders under current folder.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   file
   commands
