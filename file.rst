**git-compose.yaml** file
=========================

should contain at least **version** and a **repos** - list of 
repositores to manage. For example:

.. role:: bash(code)
   :language: bash

.. code-block:: yaml

   version: "0.1"
   repos:
     git-compose: 
       remote: https://gitlab.com/git-compose/git-compose
     git-compose-debian: 
       remote: https://gitlab.com/git-compose/git-compose-debian.git
     git-compose-vscode: 
       remote: https://gitlab.com/git-compose/git-compose-vscode
     git-compose-docs: 
       remote: https://gitlab.com/git-compose/git-compose-docs

Each repository should have at least remote URL in **remote**.
There's also optional **branch** field available. 

Branch
------

You can specify branch different from default **master**:

.. code-block:: yaml

   version: "0.1"
   repos:
     my-repo: 
       remote: https://gitlab.com/example/repository
       branch: develop

Command :bash:`gico up` would change branch in case if 
what you've specified is different from current checked
out branch in local repository.

Hooks
-----

Compose for Git allows you to describe client side git hooks
in **git-compose.yaml** file, either inlining their code
or referencing file relative to repository folder.

Read more about client side git hooks 
`here <https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks/>`_.

For example you can add `pre-push` hook with inline code like this:

.. code-block:: yaml

  version: "0.1"
  repos:
    my-repo: 
      remote: https://gitlab.com/example/repository
      hooks:
        pre-push:
          script: |
              #!/bin/bash
              echo pushing

Or from file like this:

.. code-block:: yaml

  version: "0.1"
  repos:
    my-repo: 
      remote: https://gitlab.com/example/repository
      hooks:
        pre-push:
          file: githooks/pre-push.sh

File path should be relative to repository folder, meaning that 
for the file above, you'd need a file structure like this:

.. code-block::

   project-root
   \_-git-compose.yaml
   ..\_-my-repo
   ....\_-githooks
   .......\_-pre-push.sh

Command :bash:`gico up` would take inline script, or script from 
file and place it under **.git/hooks/** setting name of file equal
to the name of hook, according to how git hooks are expected to work.

Remote alternatives
-------------------

In addition to simple way of specifying one remote location
for repository using **remote** field, you could also specify 
a several alternatives with **remotes** (note 's') field, like this:

.. code-block:: yaml

  version: "0.1"
  repos:
    my-repo:
      remotes:
        https: https://gitlab.com/my/repo.git
        ssh: git@gitlab.com:my/repo.git

Now it's up to user to define what she/he prefer by specifying it 
with CLI options **--prefer-https** or **--prefer-ssh**. 

By default the preference is **https**.

Path
----

By default **gico** uses repository name as a path to local 
folder, where it would be initially cloned and later updated.

For example with following file:

.. code-block:: yaml

  version: "0.1"
  repos:
    my-repo: 
      remote: https://gitlab.com/example/repository

Folder, where repository would be located is "./my-repo", directly
under the current directory, where **gico** was run.

.. code-block::

   project-root
   \_-git-compose.yaml
   ..\_-my-repo


However it's possible to change that using **path** field:

.. code-block:: yaml

  version: "0.1"
  repos:
    my-repo:
      path: "./custom/folder"
      remote: https://gitlab.com/example/repository


.. code-block::

   project-root
   \_-git-compose.yaml
   ..\_-custom
   ....\_-folder


